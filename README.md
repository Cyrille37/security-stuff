# Security stuff

Outils, configurations et connaissances autour de la sécurité informatique.

[[_TOC_]]

## sysctl

## PAM

## Fail2ban

### Articles

- [fail2ban with Shorewall](https://rsabalburo.blogspot.com/2014/07/fail2ban-with-shorewall.html) - 2014
- [Using fail2ban to block WordPress login attacks](https://www.bjornjohansen.com/using-fail2ban-with-wordpress) - 2014
- [System: Monitoring the fail2ban log](https://www.the-art-of-web.com/system/fail2ban-log/) - first comment 2008

### Rules


## Todo

### Détecter HTTPd scanner

Un outil de scan comme [Nikto](https://github.com/sullo/nikto) cherche des scripts dans `/cgi-bin` et autres dossiers, des backups comme `www.tar.gz` ou `/backup.zip`.

Trouver ou créer une règle qui cherche des `404` de ce genre dans le log du HTTPd car il s'agit d'un scan malveillant (*ou plus beaucoup plus rarement d'une application non-à-jour qui utilisait cette IP précédement*).

Optimisation:
- diriger les `404` du HTTPd dans un fichier dédié.
- ne pas traiter beaucoup de cas, seulement les plus fréquents/systématiques

Exemples avec des erreurs signalées par Apache
```
[core:error] AH00126: Invalid URI in request GET ////./../.../boot.ini HTTP/1.1
[core:error] AH00126: Invalid URI in request GET /DomainFiles/*//../../../../../../../../../../etc/passwd HTTP/1.1
[core:error] AH00126: Invalid URI in request GET /../../../../../../../../../../etc/passwd HTTP/1.1
[core:error] AH00126: Invalid URI in request GET /%2E%2E/%2E%2E/%2E%2E/%2E%2E/%2E%2E/windows/win.ini HTTP/1.1
[core:error] AH00126: Invalid URI in request GET /%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/etc/passwd HTTP/1.1
[core:error] AH00082: an unknown filter was not added: includes
[core:error] AH00082: an unknown filter was not added: includes
[core:error] AH00126: Invalid URI in request GET /file/../../../../../../../../etc/ HTTP/1.1
[core:error] AH00082: an unknown filter was not added: includes
[authz_core:error] AH01630: client denied by server configuration: /home/xxx/app/web/server-status
[core:error] AH00126: Invalid URI in request GET /../webserver.ini HTTP/1.1
[autoindex:error] AH01276: Cannot serve directory /usr/share/apache2/icons/: No matching DirectoryIndex (index.html,index.cgi,index.pl,index.php,index.xhtml,index.htm) found, and server-generated directory index forbidden by Options directive

```
